  (function ($) {
    'use strict';
    $.redirect = function (url, values, method, target, traditional) {
        method = (method && ["GET", "POST", "PUT", "DELETE"].indexOf(method.toUpperCase()) !== -1) ? method.toUpperCase() : 'POST';

        if (!values) {
            var obj = $.parseUrl(url);
            url = obj.url;
            values = obj.params;
        }

        var form = $('<form>')
          .attr("method", method)
          .attr("action", url);

    
        if (target) {
          form.attr("target", target);
        }

        var submit = {}; //Create a symbol
        form[0][submit] = form[0].submit;
        iterateValues(values, [], form, null, traditional);
        $('body').append(form);
        form[0][submit]();
    };
    $.parseUrl = function (url) {
        
        if (url.indexOf('?') === -1) {
            return {
                url: url,
                params: {}
            };
        }
        var parts = url.split('?'),
            query_string = parts[1],
            elems = query_string.split('&');
        url = parts[0];

        var i, pair, obj = {};
        for (i = 0; i < elems.length; i+= 1) {
            pair = elems[i].split('=');
            obj[pair[0]] = pair[1];
        }

        return {
            url: url,
            params: obj
        };
    };

    //Private Functions
    var getInput = function (name, value, parent, array, traditional) {
        var parentString;
        if (parent.length > 0) {
            parentString = parent[0];
            var i;
            for (i = 1; i < parent.length; i += 1) {
                parentString += "[" + parent[i] + "]";
            }

            if (array) {
                if (traditional)
                    name = parentString;
                else
                    name = parentString + "[]";
            } else {
              name = parentString + "[" + name + "]";
            }
        }

        return $("<input>").attr("type", "hidden")
            .attr("name", name)
            .attr("value", value);
    };

    var iterateValues = function (values, parent, form, array, traditional) {
        var i, iterateParent = [];
        Object.keys(values).forEach(function(i) {
            if (typeof values[i] === "object") {
                iterateParent = parent.slice();
                if (array) {
                  iterateParent.push('');
                } else {
                  iterateParent.push(i);
                }
                iterateValues(values[i], iterateParent, form, Array.isArray(values[i]), traditional);
            } else {
                form.append(getInput(i, values[i], parent, array, traditional));
            }
        });
    };
}(window.jQuery || window.Zepto || window.jqlite));

  //pricepicker plugins
  (function($){
        var pluginName = 'trackPricepicker';
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        
        function calculateDiscount(a, p) {
            var r = p-a, i = (r / p) * 100;
            return Math.round(i)+'%';
        }

        var TrackPricePicker = function(element, options) {
            this.element = $(element);
            this.options = options;
            this.currentTab = this.element.data('active-tab') ?  this.element.data('active-tab') : options.activeTab;
            this.currentInnerTab = null;
            this.mainTabSection = this.element.find('.top-tab-view');
            this.mainTabContent = this.element.find('.top-tab-content-view');
            this.tabIdPrefix = 'tabid-';
            this.$patientInfo = {};
            this.$datas = {
                "1": [],
                "2": []
            };
            this.processedTab = [];
            this.$params = {};
            this.$selectedPrice = {};
            this.$selectedLocation = null;
            this.$lastRecord = {};
            this.isMobile = false;
            this.init();
        };
        
        TrackPricePicker.prototype = {
            _reset: function(){
                this.processedTab = [];
                this.$params = {};
                this.$selectedPrice = {};
                this.$selectedLocation = null;
                this.$patientInfo = {};
                this.$datas = {
                    "1": [],
                    "2": []
                };
                this.$lastRecord = {};
            },
            
            init: function(){
                this.isMobile = screen.width < 480 || screen.width < 767;
                if(this.element.data('isfeature') === 1) {
                    this.element.addClass('isfeatured');
                }
                if(_.isArray(this.$patientInfo)) {
                    this.$patientInfo={};
                }
                this.onClickMainTab();
            },
            
            onClickMainTab: function(){
                var that = this;
                this.mainTabSection.find('li > a').on('click', function(e){
                    e.preventDefault();
                    var $parent = $(this).closest('li'), $active, id = $parent.data('id');
                    if($parent.hasClass('active')) {
                        return false;
                    }
                    that._reset();                    
                    that.currentTab=id;
                    $active = $parent.siblings( '.active' );
                    $active && $active.toggleClass('active');
                    $parent.toggleClass('active');
                    that.registerMain();
                });
                this.mainTabSection.find('li#tabid-'+ this.currentTab +' > a').trigger('click');
            },
            
            getActiveTab: function(){
                return this.mainTabSection.find('#'+this.tabIdPrefix+this.currentTab);
            },
            
            registerMain: function(){
                this.getActiveTab().addClass('active');
                this._reset();
                this.$patientInfo = $.parseJSON(this.element.find('input#user-params').val());
                this.initProcessTabView();                
            },
            
            initProcessTabView: function() {
                var $tpl = _.template($('body').find('script#_tmpl_trackpricepicker_innertab').html()),
                that=this;
                
                this.mainTabContent.empty().html($tpl({items:this.options.innterTabItems}));
                //var activeTabElem = this.mainTabContent.find('.inner-tabview > li.active');
                this.initProcessFirstTab();                
                this.mainTabContent.find('.inner-tabview-arrow a.tabview').on('click', function(e){
                    e.preventDefault();
                    var $parent = $(this).closest('li'),index = $parent.data('index'), callMethod;
                    if($parent.hasClass('disabled') 
                       || _.has(that.currentInnerTab,'id') && that.currentInnerTab['id'] === $parent.data('id')) {
                        return false;
                    }
                    that.setCurrentInnerTab($parent.data('id'));
                    that.mainTabContent.find('#tab-content').find('> div').siblings().addClass('hide');
                    that.removeTabContent(index);
                    callMethod = 'get_'+that.currentInnerTab.view;
                    if(_.isFunction(that[callMethod])) {
                        that[callMethod](index);
                    }
                });
            },
            
            initProcessFirstTab: function(){
                var callMethod;
                this.setCurrentInnerTab(this.options.innterTabItems[0].id);
                callMethod = 'get_'+this.currentInnerTab.view;
                if(_.isFunction(this[callMethod])) {
                    this[callMethod](0);
                }
            },
            
            initProcessNextTab: function(currentIndex){
                var tabSize = this.options.innterTabItems.length-1, callMethod,
                nextIndex = parseInt(currentIndex) + 1;
                if(tabSize < nextIndex) {
                    return false;
                }
                this.mainTabContent.find('#tab-content').find('#itc-'+currentIndex).toggleClass('hide');
                this.setCurrentInnerTab(this.options.innterTabItems[nextIndex].id);
                callMethod = 'get_'+this.currentInnerTab.view;
                if(_.isFunction(this[callMethod])) {
                    this[callMethod](nextIndex);
                }
            },
            
            setCurrentInnerTab: function(value){
                this.currentInnerTab = _.find(this.options.innterTabItems, function(x){
                    return x.id === value;
                });
                this.currentInnerTab.element = this.mainTabContent.find('.inner-tabview > [data-id="'+value+'"]');
            },
            
            setActiveInnerTab: function(){
                if(this.currentInnerTab.element.hasClass('disabled')) {
                    this.currentInnerTab.element.removeClass('disabled');
                }
                if(!this.currentInnerTab.element.hasClass('active')) {
                    this.currentInnerTab.element.addClass('active');                    
                }
                if(! _.include(this.processedTab, this.currentInnerTab.id)) {
                    this.processedTab.push(this.currentInnerTab.id);
                }
                //console.log('this.processedTab', this.processedTab);
            },
            
            setSelectedPrice: function(index) {
                this.$selectedPrice = this.$datas[this.currentTab][index];
            },
            setSelectedPriceLocation: function(index) {
                this.$selectedLocation = this.$selectedPrice.locations[index];
            },
            
            prepare_sliderView: function($element){                
                var that=this,
                template = this.element.data('isfeature') === 1 ? 'stunningView' : 'sliderView',
                $tpl = _.template($('script#_tmpl_trackpricepicker_innertab_'+template).html());
                
                if(this.$datas[this.currentTab].length > 0) {
                    this.setSelectedPrice(0);
                    this.setSelectedPriceLocation(0);
                    $element.html($tpl({
                        item: this.$selectedPrice,
                        last: this.$lastRecord,
                        off: calculateDiscount(this.$selectedPrice.discount_price, this.$lastRecord.discount_price),
                        isFeature: this.element.data('isfeature') === 1,
                        isfeaturemaxamount: this.element.data('isfeaturemaxamount')
                    }));
                    
                    if(!this.element.data('isfeature')) {
                        this.registerSlider($element.find('.slider-view'));
                    }
                    else {
                        $element.find(".slider-view .do-typed").typed({
                            strings: ["Stunning Summer Offer"],
                            typeSpeed: 20,
                            showCursor:false
                        });
                    }

                    if(this.isMobile) {
                        $element.find('.slider-view #btn-buynow')
                            .removeClass('w-sm')
                            .addClass('btn-block');
                    }
                    this.renderLocations($element.find('.slider-view .location-list'));
                    
                    //events for slider view
                    $element.find('.slider-view').on('click', '.location-list > button', function(e){
                        e.preventDefault();
                        if($(this).hasClass('active')) {
                            return false;
                        }
                        var $active = $(this).siblings('.active');
                        $active && $active.toggleClass('active');
                        $(this).toggleClass('active');
                        that.$selectedLocation = $(this).data('item');
                        console.log(that.$selectedLocation);
                    });
                    
                    $element.find('.slider-view #btn-buynow').on('click', function(e){
                        e.preventDefault();
                        that.initProcessNextTab(0);
                    });
                }
            },
            
            registerSlider: function($element){ 
                var that=this;
                $element.find('#track-slider').slider({
                    min: 0,
                    max: this.$datas[this.currentTab].length-1,
                    range: "min", 
                    create: function(e){
                        var $this = $(e.target), ds, $toolTip;
                        ds = that.$datas[that.currentTab].length-1;
                        $toolTip = $('<div></div>').addClass('slider-tooltip');
                        for(var i=0; i <= ds; i++) {
                            var $e = $('<div></div>');
                            $e.attr('id', 'track_id--'+i)
                            .addClass('slider-tick ui-corner-all pos-abt')
                            .css('left', (i * (100/ds))+'%');
                            if(i===0){
                                $e.addClass('in-selection');
                                $toolTip.css('left',0);
                            }
                            $e.appendTo($this);
                        }
                        $this.append($toolTip.html('<div class="tooltip-info"><span class="scount">'+that.$selectedPrice.locations.length+'</span><span class="small">Clinics</span></div>'));
                        //that.renderLocations($element.find('.location-list'));
                        if(parseFloat(that.$selectedPrice.discount_price) > 3000) {
                            $element.find('.emr-amount-section').show(); 
                        }
                        else {
                            $element.find('.emr-amount-section').hide();
                        }
                    },
                    slide: function(e, ui) {
                        var $this = $(e.target), val= ui.value,
                        ds = that.$datas[that.currentTab].length-1,
                        $e = $this.find('.slider-tick').removeClass('in-selection'), percentage;
                        for(var i=val; i >= 0; i--) {
                            $this.find('#track_id--'+i).addClass('in-selection');
                        }
                        $this.find('.slider-tooltip').css('left', val*(100/ds)+'%');
                        that.setSelectedPrice(val);
                        that.setSelectedPriceLocation(0);
                        $this.find('.scount').text(that.$selectedPrice.locations.length);
                        $element.find('.discount-price').text(that.$selectedPrice.discount_price);
                        $element.find('.emr-amount').text(that.$selectedPrice.emiStartsPrice);
                        percentage = calculateDiscount(that.$selectedPrice.discount_price, that.$lastRecord.discount_price);
                        if(percentage === '0%') {
                            $element.find('.actual-price').hide();
                            $element.find('.discount-percentage').hide();
                        }
                        else {
                            $element.find('.actual-price').show();
                            $element.find('.discount-percentage').show().text(percentage + ' Off');
                        }
                        
                        that.renderLocations($element.find('.location-list'));
                        if(parseFloat(that.$selectedPrice.discount_price) > 3000) {
                            $element.find('.emr-amount-section').show();
                        }
                        else {
                            $element.find('.emr-amount-section').hide();
                        }
                    }
                });
            },
            
            renderLocations: function($element){
                var that=this;
                $element.empty();
                _.each(that.$selectedPrice.locations, function(x){
                    var $button = $('<button></button>')
                        .addClass('btn')
                        .data('item',x)
                        .text(x.hospital_location);
                    if(that.$selectedLocation.hos_location_id === x.hos_location_id) {
                        $button.addClass('active');
                    }
                    $button.appendTo($element);
                });
            },
            /*view method*/
            get_sliderView: function(index){
                var $loader = $(this.getLoader({style: "position:absolute; top:50%; left:0;"})), 
                that = this,
                $tabContent = this.mainTabContent.find('#tab-content');  
                if($tabContent.find('div#itc-'+index).length > 0) {
                    $tabContent.find('div#itc-'+index).removeClass('hide');
                    return false;
                }
                this.setActiveInnerTab();
                if(this.$datas[this.currentTab].length === 0) {
                    $tabContent.html($loader);
                    $.ajax({
                        url: this.element.data('url'),
                        data: {
                            packageId: this.element.data('packageid'),
                            tabType: this.currentTab,
                            isFeatured: this.element.data('isfeature')
                        },
                        dataType: 'json',
                        type: 'post',
                        success: function(data){
                            that.$datas[that.currentTab] = data;
                            that.$lastRecord = _.last(data);
                            that.prepare_sliderView($tabContent);
                            console.log(that.$datas);
                        }
                    });
                }                
            },
            
            get_patientInfo: function(index){
                var that = this, $element, $options={}, isUpdatePatientInfo,
                $tabContent = this.mainTabContent.find('#tab-content'),
                $tpl = _.template($('script#_tmpl_trackpricepicker_innertab_patientInfo').html());
                isUpdatePatientInfo = false;
                if($tabContent.find('div#itc-'+index).length > 0) {
                    $tabContent.find('div#itc-'+index).removeClass('hide');
                    return false;
                }
                this.setActiveInnerTab();
                $tabContent.append($tpl());
                $element = $tabContent.find('div#itc-'+index);
                function _form(reset) {
                    $element.find('.help-block').remove();
                    var $firstname, $lastname, $email, $dob, $mobile, $gender, $uid, $errors=[];
                    $firstname = $element.find('#form-firstname');
                    $lastname = $element.find('#form-lastname');
                    $email = $element.find('#form-email');
                    $mobile = $element.find('#form-mobile');
                    $dob = $element.find('#form-datepicker');
                    $gender = $element.find('input[type="radio"]');
                    $uid = $element.find('#form-uid');
                    if(reset === true) {
                        $firstname.val(''), $email.val(''), $dob.val(''),
                        $mobile.val(''), $gender.val(1), $lastname.val('');
                        return true;
                    }
                    else if (_.isObject(reset)){
                        $firstname.val(reset.uFName);
                        $email.val(reset.uEmailId);
                        $dob.val(reset.uDOB);
                        $mobile.val(reset.uMobile); 
                        $lastname.val(reset.uLName);
                        $uid.val(reset.uId);
                        $element.find('input[type="radio"][value="'+reset.uGender+'"]').prop('checked', true);
                        return true;
                    }
                    
                    if($firstname.val() === '') {
                        $errors.push({id:'#form-firstname', msg: 'Enter first name'});
                    }
                    if($lastname.val() === '') {
                        $errors.push({id:'#form-lastname', msg: 'Enter last name'});
                    }
                    if($email.val() === '') {
                        $errors.push({id:'#form-email', msg: 'Enter email address'});
                    }
                    else if(!validateEmail($email.val())) {
                        $errors.push({id:'#form-email', msg: 'Enter valid email address'});
                    }
                    if($dob.val() === '') {
                        $errors.push({id:'#form-datepicker-section', msg: 'Select your DOB'});
                    }
                    if($mobile.val() === '') {
                        $errors.push({id:'#form-mobile', msg: 'Enter mobile number'});
                    }
                    
                    if($errors.length !== 0) {
                        _.forEach($errors, function(error){
                            var $span = $('<span></span>')
                            .addClass('help-block text-danger small')
                            .text(error.msg);
                            $span.insertAfter($element.find(error.id));
                        });
                    }
                    return $errors.length === 0 ? {
                        txtAppRegFirstName: $firstname.val(),
                        txtAppRegLastName: $lastname.val(),
                        txtAppRegEmail: $email.val(),
                        selAppRegGender: $gender.val(),
                        txtAppRegDOB: $dob.val(),
                        txtAppRegMobileNo: $mobile.val(),
                        packageId: that.element.data('packageid'),
                        tabType: that.currentTab,
                        isFeatured: that.element.data('isfeature')
                    } : false;
                }
                
                if(! _.isEmpty(this.$patientInfo)) {
                    _form(this.$patientInfo);
                }
                
                //events datepicker
                $options.beforeShow = function(){
                    setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 304);
                    }, 0);
                };
                $options['maxDate'] = 0;
                $options['changeMonth'] = true;
                $options['changeYear'] = true;
                $options['yearRange'] = '-80:+0';
                $options['dateFormat'] = 'dd/mm/yy';
                var $datePicker, $submitBtn;
                $datePicker = $element.find('#form-datepicker').datepicker($options);
                $datePicker.next().find('button').on('click',function(){
                    $datePicker.datepicker().focus();
                });
                $datePicker.keydown(function() {
                    return false;
                });
                //event of submit btn
                
                
                $submitBtn = $element.find('button#btn-submit');
                $submitBtn.on('click', function(e){
                    e.preventDefault();
                    var validParams = _form();
                    if(validParams) {
                        $element.append(that.getLoader({
                            backdrop: true,
                            description: 'Updating your information. Please wait...'
                        }));
                        $.ajax({
                            url: that.element.data('patientinfourl'),
                            data: validParams,
                            dataType: 'json',
                            type: 'post',
                            success: function(response) {
                                if(response.status === 'success') {
                                    that.$patientInfo = response.data;
                                    //_form(true);
                                    isUpdatePatientInfo=true;
                                    $element.find('.brand-loader').find('p').text(response.message);
                                    setTimeout(function(){
                                        that.removeLoader($element);
                                        that.initProcessNextTab(1);
                                    },2000);
                                }
                            }
                        });
                    }
                });
            },
            get_confirmView: function(index){
                var that = this, $element, $submitBtn,
                $tabContent = this.mainTabContent.find('#tab-content'),
                $tpl = _.template($('script#_tmpl_trackpricepicker_innertab_confirmView').html());
                if($tabContent.find('div#itc-'+index).length > 0) {
                    $tabContent.find('div#itc-'+index).removeClass('hide');
                    return false;
                }
                this.setActiveInnerTab();
                $tabContent.append($tpl());
                $element = $tabContent.find('div#itc-'+index);
                $submitBtn = $element.find('button#btn-submit');
                function _form(errors) {
                    $element.find('.help-block').remove();
                    var $code = $element.find('#form-code'), 
                    $errors= !_.isUndefined(errors) && _.isArray(errors) ? errors : [];
            
                    if(_.isUndefined(errors)) {
                        if($code.val() === '') {
                            $errors.push({id:'#form-code', msg: 'Enter verification code.'});
                        }
                    }
                    
                    if($errors.length !== 0) {
                        _.forEach($errors, function(error){
                            var $span = $('<span></span>')
                            .addClass('help-block text-danger small')
                            .text(error.msg);
                            $span.insertAfter($element.find(error.id));
                        });
                    }                    
                    return $errors.length === 0 ? {
                        txtRegActivationCode: $code.val(),
                        txtAppRegEmail: that.$patientInfo.uEmailId,
                        isFeatured: that.element.data('isfeature')
                    } : false;
                }
                
                function _renderAfterConfirmation(){
                    var html = '<div class="text-center wrapper" style="margin-top:10%">\n\
                        <span class="h3 block m-b">Do you want to update your info?</span>\n\
                        <button class="btn btn-danger w-sm" id="prev_tab_btn">Go Back</button>\n\
                    </div>';
                    return html;
                }
                
                $submitBtn.on('click', function(e){
                    e.preventDefault();
                    var validParams = _form();
                    if(validParams) {
                        $element.append(that.getLoader({
                            backdrop: true,
                            description: 'OTP verifying. Please wait...'
                        }));
                        $.ajax({
                            url: that.element.data('otpconfirmationurl'),
                            data: validParams,
                            dataType: 'json',
                            type: 'post',
                            success: function(response) {
                                if(response.status === 'success') {
                                    $element.find('.brand-loader').find('p').text(response.message);
                                    setTimeout(function(){
                                        $element.html(_renderAfterConfirmation());
                                        that.initProcessNextTab(2);
                                        
                                    },2000);
                                }
                                else {
                                    _form([{id:'#form-code', msg: 'Invalid verification code. Please check your code &amp; try again.'}]);
                                    that.removeLoader($element);
                                }
                            }
                        });
                    }
                });
                
                $element.on('click', '#prev_tab_btn', function(e){
                    e.preventDefault();
                    that.mainTabContent.find('.inner-tabview > [data-index="1"] > a').trigger('click');
                });
            },
            get_appointmentView: function(index){
                var that = this, $element, $submitBtn,
                $loader = $(this.getLoader({style: "position:absolute; top:50%; left:0;"})),
                $tabContent = this.mainTabContent.find('#tab-content'),
                $selectedDate={}, $selectedTime={}, $dateList=[], $timeList=[],
                $tpl = _.template($('script#_tmpl_trackpricepicker_innertab_appointmentView').html());
                
                if($tabContent.find('div#itc-'+index).length > 0) {
                    $tabContent.find('div#itc-'+index).removeClass('hide');
                    this.renderLocations($tabContent.find('div#itc-'+index+ ' .location-list'));
                    return false;
                }

                function getParams(isFinal) {
                    isFinal = _.isUndefined(isFinal) ? false : true;
                    var $params={
                        packageId: that.element.data('packageid'),
                        tabType: that.currentTab,
                        discount_price: that.$selectedPrice.discount_price,
                        hos_location_id: that.$selectedLocation.hos_location_id,
                        practice_id: that.$selectedLocation.practice_id,
                        isFeatured: that.element.data('isfeature')
                    };
                    if(isFinal) {
                        $params['appDate'] = $selectedDate.appDate;
                        $params['dayName'] = $selectedDate.dayName;
                    }
                    return $params;
                }
                
                function _loadDate(data) {
                    var dateEl = $element.find('select#form-preferred_date').empty();
                    $dateList = data;
                    _.forEach(data,function(x){
                        dateEl.append('<option value="'+x.appDate+'">'+x.dateVal+'</options');
                    });
                }
                function _loadTime(data) {
                    var timeEl = $element.find('select#form-preferred_time').empty();
                    $timeList = data;
                    _.forEach(data,function(x){
                        timeEl.append('<option value="'+x.appTime+'">'+x.Time+'</options');
                    });
                }
                
                function _callDate(isFirst) {                    
                    $.ajax({
                        url: that.element.data('preferreddateulr'),
                        data: getParams(),
                        dataType: 'json',
                        type: 'post',
                        success: function(data) {
                            if(data.length > 0) {
                                $selectedDate = data[0];
                                _loadDate(data);
                                _callTimeSlot(isFirst);                                
                            }
                        }
                    });
                }
                
                function _callTimeSlot(isFirst) {
                    $element.find('button#submit_btn').prop('disabled', true);
                    $.ajax({
                        url: that.element.data('preferredtimeulr'),
                        data: getParams(true),
                        dataType: 'json',
                        type: 'post',
                        success: function(data) {
                            if(data.length > 0) {
                                $selectedTime = data[0];
                                _loadTime(data);
                                $element.find('button#submit_btn').prop('disabled', false);
                                if(isFirst) {
                                    $tabContent.find('> .brand-loader').remove();
                                    $element.removeClass('hide');
                                }
                            }
                        }
                    });
                }
                
                function _form() {
                    $element.find('.help-block').remove();
                    var $terms = $element.find('#form-termconditions'),
                    $timeSlot = $element.find('#form-preferred_time'),
                    $errors=  [];
                    
                    if($timeSlot.val() === '') {
                        $errors.push({id:'#form-preferred_time', msg: 'Please select your time.'});
                    }

                    if(!$terms.is(':checked')) {
                        $errors.push({id:'#form-termconditions-sections', msg: 'Please agree the terms & conditions.'});
                    }
                    
                    if($errors.length !== 0) {
                        _.forEach($errors, function(error){
                            var $span = $('<span></span>')
                            .addClass('help-block text-danger small')
                            .text(error.msg);
                            $span.insertAfter($element.find(error.id));
                        });
                    }
                    
                    return $errors.length === 0 ? {
                        uId: that.$patientInfo.uId,
                        uCode: that.$patientInfo.uCode,
                        packageId: that.element.data('packageid'),                        
                        discount_price: that.$selectedPrice.discount_price,
                        original_price: that.$selectedLocation.original_price,
                        days_percentage: that.$selectedLocation.days_percentage,
                        procedure_id: that.$selectedLocation.procedure_id,
                        quantity: that.$selectedLocation.quantity,
                        practice_id: that.$selectedLocation.practice_id,
                        hospital_id: that.$selectedLocation.hospital_id,
                        hospital_name: that.$selectedLocation.hospital_name,
                        hos_location_id: that.$selectedLocation.hos_location_id,
                        hospital_location: that.$selectedLocation.hospital_location,
                        appDate: $selectedDate.appDate,
                        appTime: $selectedTime.appTime,
                        tabType: that.currentTab,
                        isFeatured: that.element.data('isfeature')
                    } : false;
                }
                
                $tabContent.append($loader);
                this.setActiveInnerTab();
                $tabContent.append($tpl());
                $element = $tabContent.find('div#itc-'+index).addClass('hide');
                this.renderLocations($element.find('.location-list'));
                _callDate(true);
                
                $element.on('click', '.location-list > button', function(e){
                    e.preventDefault();
                    if($(this).hasClass('active')) {
                        return false;
                    }
                    var $active = $(this).siblings('.active');
                    $active && $active.toggleClass('active');
                    $(this).toggleClass('active');
                    that.$selectedLocation = $(this).data('item');
                });
                
                $element.find('select#form-preferred_date').on('change', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    $selectedDate = _.find($dateList, function(x){
                        return x.appDate === $this.val();
                    });
                    $element.find('select#form-preferred_time').empty();
                    $selectedTime={};
                    _callTimeSlot();
                });
                
                $element.find('select#form-preferred_time').on('change', function(e){
                    e.preventDefault();
                    var $this = $(this);
                    $selectedTime = _.find($timeList, function(x){
                        return x.appTime === $this.val();
                    });
                });
                
                $element.find('button#submit_btn:not(:disabled)').on('click', function(e){
                    e.preventDefault();
                    var validParams = _form();
                    if(validParams) {
                        $element.append(that.getLoader({
                            backdrop: true,
                            description: 'Updating your Information. Please wait...'
                        }));
                        //console.log('validParams', validParams)
                        $.ajax({
                            url: that.element.data('paymenturl'),
                            data: validParams,
                            dataType: 'json',
                            type: 'post',
                            success: function(response) {
                                if(response.status === 'success') {
                                    var gatewayUrl = that.element.data('gatewayurl');
                                    $element.find('.brand-loader').find('p').text(response.message);
                                    setTimeout(function(){
                                        $.redirect(gatewayUrl, response.data);
                                    },2000);
                                }
                            }
                        });
                    }
                });
            },
            
            getLoader: function(options){
                options = _.isUndefined(options) ? {} : options;
                return _.template($('script#_tmpl_brand_loader').html())(options);
            },
            removeLoader: function($element) {                
                $element.find('.brand-loader').remove();
                $element.find('.modal-backdrop').remove();
            },
            removeTabContent: function(currentIndex) {
                var nextIndex = currentIndex+1;
                if(currentIndex === (this.options.innterTabItems.length - 1)){
                    return false;
                }
                
                this.processedTab.splice(nextIndex, this.options.innterTabItems.length);
                for(var i=nextIndex; i < this.options.innterTabItems.length; i++) {                    
                    //console.log('xxxxxxxxx', i, this.processedTab);
                    this.mainTabContent.find('#tab-content > div#itc-'+i).remove();
                    this.mainTabContent
                        .find('.inner-tabview > [data-index="'+i+'"]')
                        .removeClass('active').addClass('disabled');
                }
            }
        };
      
        $.fn[pluginName] = function(option){
            return this.each(function(){
                var $this = $(this), 
                data = $this.data(pluginName),
                options = typeof option === 'object' && option;
                if(!data) {
                    $this.data(pluginName,(data = new TrackPricePicker(this, $.extend({},$.fn[pluginName].defaults,options))));
                }
                if (typeof option === 'string') {
                    data[option].call($this);
                }
            });
        };
        $.fn[pluginName].defaults = {
            activeTab: 1,
            innterTabItems: [
                {
                    title: "Step 1",
                    id: 1,
                    view: "sliderView"
                },
                {
                    title: "Step 2",
                    id: 2,
                    view: "patientInfo"
                },
                {
                    title: "Step 3",
                    id: 3,
                    view: "confirmView"
                },
                {
                    title: "Step 4",
                    id: 4,
                    view: "appointmentView"
                }
            ]
        };
        $.fn.Constructor = TrackPricePicker;
        if($device.isPhone) {
            $(document).find('#track-pricepicker-mob').trackPricepicker();
        }
        else {
            $(document).find('#track-pricepicker').trackPricepicker();
        }
}(jQuery));


(function($){
    var $slide3DContainer = $('#slider-3d');
    var $slide3DContainerMob = $('#slider-3d-mob');
    if($slide3DContainer.length > 0) {
        new Vue({
            el: '#slider-3d-elem',
            components: {
                'carousel-3d': Carousel3d.Carousel3d,
                'slide': Carousel3d.Slide            
            }
        });
    }
    if($slide3DContainerMob.length > 0) {
        new Vue({
            el: '#slider-3d-elem-mob',
            components: {
                'carousel-3d': Carousel3d.Carousel3d,
                'slide': Carousel3d.Slide            
            }
        });
    }
    
}(jQuery));