//plugins
(function($){
    var pluginName = 'starDatatable';
    
    var StarDataTablePlugin = function(element, options) {
        this.element = $(element);
        this.options = options;
        this.panel = $('<div class="panel"></div>');
        this.gridContainer = $('<div class="data-table-container"></div>');
        this.title = options.title;
        this.columnCount = options.columns.length;
        this.init();
    }
    
    StarDataTablePlugin.prototype = {
        init: function() {
            this.element.empty().append(this.renderPanel(this.getData.bind(this)));
        },
        
        getData: function() {
            var that = this;
            $.ajax({
                url: this.options.apiUrl,
                type: 'GET',
                dataType: 'JSON',
                success: function(response) {
                    that.panel.find('.panel-body')
                    .empty()
                    .append(that.renderGrid(response));
                }
            });
        },
        
        //render methods
        renderPanel: function( callback ) {
            this.panel.addClass(this.options.panelStyle);
            if(this.options.showPanelHeader) {
                this.panel.append(this.renderPanelHeader());
            }            
            this.panel.append(this.renderBody());            
            if(this.options.showPanelFooter) {
                this.panel.append(this.renderPanelFooter());
            }
            this.element.empty().append(this.panel);
            
            if(typeof callback === 'function') {
                callback();
            }
        },
        
        renderPanelHeader: function() {
            return '<div class="panel-heading">'+this.title+'</div>';
        },
        
        renderPanelFooter: function() {
            return '<div class="panel-footer">Footer</div>';
        },
        
        renderBody: function() {
            return '<div class="panel-body">Loading....</div>';
        },
        
        renderGrid: function(data) {
            var table = $('<table class="table"></table>').addClass(this.options.tableClassName);
            if(this.columnCount > 0) {
                table.append(this.renderGridColumn());                
                table.append(this.renderGridRow(data));
            }
            return this.gridContainer.append(table);
        },
        
        renderGridColumn: function() {
            var thead = $('<tr></tr>');
            $.each(this.options.columns, function(key, column){
                var th = $('<th></th>');
                if(column.type && column.type === 'action') {
                    th.text(column.title); 
                }
                else {
                    th.text(column.title);                
                }
                thead.append(th);
            });
            return thead;
        },
        
        renderGridRow: function(data) {
            var self = this;
            var tbody = $('<tbody></tbody>');
            $.each(data, function(rowIndex, row){
                var tr = $('<tr></tr>');
                $.each(self.options.columns, function(key, column){
                    var td = $('<td></td>');
                    if(column.type && column.type === 'action') {
                        td.append(self.renderButtons(column.buttons, td, row, rowIndex));
                    }
                    else {
                        td.text(row[column.dataIndex]);
                    }                    
                    tr.append(td);
                }); 
                tbody.append(tr);
            });
            return tbody;
        },
        
        renderButtons: function(buttons, element, row, rowIndex) {
            var actions = $('<div class="btn-actions"></div>');
            $.each(buttons, function(k, btn) {
                var $btn = $('<button/>').addClass('btn btn-sm').text(btn.text)
                    .on('click', function(e){
                        e.preventDefault();
                        btn.callback(element, row, rowIndex);
                    });
                actions.append($btn);
            });
            return actions;
        }
    };
    
    //binding with jquery function
    $.fn.starDatatable = function(option) {
        return this.each(function(){
            var $this = $(this), 
            data = $this.data(pluginName),
            options = typeof option === 'object' && option;
            if(!data) {
                $this.data(pluginName,(data = new StarDataTablePlugin(this, $.extend({},$.fn[pluginName].defaults,options))));
            }
            if (typeof option === 'string') {
                data[option].call($this);
            }
        });
    };
    
    //define default options
    $.fn[pluginName].defaults = {
        columns: [],
        data: null,
        showPanelHeader: true,
        showPanelFooter: false,
        stickyOnHeader: false,
        title: 'Student List View (DataTable)',
        panelStyle: 'panel-default',
        apiUrl: '/ui-session/data/grid_data.json',
        tableClassName: 'table-striped',
        actions: []
    };
}(jQuery));