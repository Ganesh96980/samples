/*Assessment data*/
var _validateAssessmentData = function(data) {
    if(_.isUndefined(data)) {
        return {};
    }
    var items = _.each(data.items, function(x,y){
        if(_.isString(x.qid)) {
            x.qid = parseInt(x.qid);
        }
        if(_.isString(x.order)) {
            x.order = parseInt(x.order);
        }
        if(_.isString(x.isNested)) {
            x.isNested = parseInt(x.isNested);
        }
        _.each(x.items, function(item,k){
            if(_.isString(item.id)) {
                item.id = parseInt(item.id);
            }
            if(_.isString(item.qid)) {
                item.qid = parseInt(item.qid);
            }
        });
    });
    data.items = items;
    return data;
};
+function($) {
    'use strict';
    
    var pluginName = 'dpAssessment';
    
    var template = {
        tpl_header: _.template($('script#_tmpl_assessment_header').html()),
        tpl_body: '<div class="panel-body"></div>',
        tpl_container: _.template($('script#_tmpl_assessment_row').html()),
        tpl_nested_container: _.template($('script#_tmpl_assessment_nested_row').html()),
        inputs: {
            'text': function(data) {
                var $text = $('<input/>').addClass('form-control');
                $text.attr('type','text');
                $text.val('');
                //$text.attr('id',data.label.replace(/ /g,"_").toLowerCase());
                $text.attr('id',data.id);
                if(data.disabled === true) {
                    $text.prop('disabled', true);
                }
                $text.data('answerIndex',data.index);
                if(data.onChange && _.isFunction(data.onChange)) {
                    $text.change(data.onChange);
                }
                return $text;
            },
            'dropdown': function(data) {
                var $dropdown = $('<select></select>').addClass('form-control');
                $dropdown.val('');
                $dropdown.attr('id','question-input-id-'+data.id);
                $dropdown.data('item',data);
                if(data.disabled === true) {
                    $dropdown.prop('disabled', true);
                }
                $dropdown.append($('<option></option>').attr('value','').text(''));
                $.each(data.items,function(k,v){
                    $('<option></option>')
                        .attr('value', v.id)
                        .text(v.title)
                        .data('item',v).appendTo($dropdown);
                });
                if(data.onChange && _.isFunction(data.onChange)) {
                    $dropdown.change(data.onChange);
                }
                return $dropdown;
            },
            imageradiolist: function(data) {
                var $imageradiolist,
                tpl = _.template($('script#_tmpl_input_imageradiolist').html());
                $imageradiolist = $(tpl(data));
                $imageradiolist.data('item',data);
                _.each(data.items, function(item, index){
                    $imageradiolist.find('input#imageradiolist-id-'+item.id).data('item',item);
                });
                return $imageradiolist;
            },
            radiolist: function(data) {
                var $radiolist,
                tpl = _.template($('script#_tmpl_input_radiolist').html());
                $radiolist = $(tpl(data));
                $radiolist.data('item',data);
                _.each(data.items, function(item, index){
                    $radiolist.find('input#radiolist-id-'+item.id).data('item',item);
                });
                return $radiolist;
            },
            range: function(data) {
                var $range,
                tpl = _.template($('script#_tmpl_input_range').html());
                $range = $(tpl(data));
                $range.data('item',data);
                _.each(data.items, function(item, index){
                    var $rangeInput = $range.find('div#range-'+item.id).data('item',item);
                    $rangeInput.slider({
                        range: "min",
                        animate: "fast",
                        min: item.value[0],
                        max: item.value[1],
                        value: item.value[0],
                        step:100,
                        create: function(){
                            var $p = $(this).closest('#input-range-ans-'+item.id);
                            $p.find('#range-value > span').text('Rs. '+ item.value[0]);
                        },
                        slide: function(e,ui){
                            var $p = $(this).closest('#input-range-ans-'+item.id);
                            $p.find('#range-value > span').text('Rs. '+ ui.value);
                        }
                    });
                });
                return $range;
            }
        }
    };
    
    var Assessment = function(element, options) {
        this.element = $(element);
        this.options = options;
        this.$body = null;
        this.$footer = null;
        this.name = options.from;
        this.$data = options.data;
        this.id = options.data.solution_configurator_id;
        this.packageId = options.data.id;
        this.totalItems = this.options.data.length;
        this.questionItems={};
        this.nextIndexs=[];
        this.resultData={};
        this._resolveQuestions();
        this.init();
        this.errors=[];
        console.log('data',this.$data)
    };
    
    Assessment.prototype = {
        init: function() {
            var $that = this;
            if(this.options.enableScroll === true) {
                setTimeout(function(){
                    $('html,body').animate({
                        scrollTop: $that.element.offset().top - $that.options.scrollTopOffset
                    }, 2000);
                },100);
            }
            this.element.on('change','.input-imageradiolist input[type="radio"]', function(){
                var $this = $(this), $parent = $this.closest('.item'), $active;
                $active = $parent.siblings( ".active" );
                $active && $active.toggleClass('active');
                $parent.toggleClass('active');
            });
            
            this.element.empty();
            this._renderTitle();
            this._renderBody();
            this._renderFooter();
            this._renderItem(true);
        },
        
        getInstance: function() {
            return this;
        },
        
        _resolveQuestions: function() {
            var data = _.indexBy(this.$data.items,'qid'), a, _removableIndex=[], final;
            a = _.chain(data)
                .each(function(q) {
                    _.each(q.items, function(opts){                        
                        if(opts.qid !== 0) {
                            opts.nextQuestion = data[opts.qid];
                            _removableIndex.push(opts.qid);
                        }
                        else {                            
                            opts.nextQuestion = false;
                        }
                    });
                })
                .value();
            _.remove(a,_removableIndex);
            this.questionItems = _.chain(a)
            .sortBy(function(x){
                return x.order;
            })
            .value();
            this.nextIndexs = _.keys(this.questionItems);
        },
        
        _validationEmail: function(element) {
            var $value = $.trim($(element).val());
            if($value === "") {
                this.errors.push({
                    'msg': 'Please enter your email address',
                    'element': element
                });
            }
            else {
                var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if(!pattern.test($value)) {
                    this.errors.push({
                        'msg': 'Please enter your valid email address',
                        'element': element
                    });
                }
            }
        },
        
        _validationPhone: function(element) {
            var $value = $.trim($(element).val());
            if($value === "") {
                this.errors.push({
                    'msg': 'Please enter your Phone Number',
                    'element': element
                });
            }
            else {
                var pattern = /^[0-9-+]+$/;
                if(!pattern.test($value)) {
                    this.errors.push({
                        'msg': 'Please enter your valid Phone Number',
                        'element': element
                    });
                }
            }
        },
        
        _validationName: function(element) {
            var $value = $.trim($(element).val());
            if($value === "") {
                this.errors.push({
                    'msg': 'Please enter your Name',
                    'element': element
                });
            }
            else {
                var pattern = /^[a-zA-Z]{3,16}$/;
                if(!pattern.test($value)) {
                    this.errors.push({
                        'msg': 'Please enter valid Name',
                        'element': element
                    });
                }
            }
        },
        
        _hasErrors: function() {
            return this.errors.length > 0;
        },
        
        _removeIndex: function(currentIndex) {
            this.nextIndexs = _.reject(this.nextIndexs,function(x){
               return x === currentIndex; 
            });
        },
        _renderTitle: function() {
            var data=this.options.data;
            if(data.title) {
                this.element.append(template.tpl_header({data:data}));                
            }
        },
        _renderBody: function() {
            this.element.append('<div class="panel-body _input-default"></div>');
            this.$body = this.element.find('.panel-body');
        },
        _renderFooter: function() {
            this.element.append('<div class="panel-footer m-t-md"></div>');
            this.$footer = this.element.find('.panel-footer');
        },
        _renderSubmitSection: function() {
            var $submitSection = this.$footer.find('.assessment-submit-section'),
                $$ = this;
            if($submitSection.length === 0) {
                var submitSection = _.template($('script#_tmpl_assessment_submit_container').html());
                $submitSection = $(submitSection());
                var $submitBtn = $submitSection.find('button#result-submit');
                $submitBtn.prop('disabled',true);
                
                $submitSection.on('change','input#result-term',function(e){
                    e.preventDefault();
                    if($(this).is(':checked')) {
                        $submitBtn.prop('disabled',false);
                    }
                    else {
                        $submitBtn.prop('disabled',true);
                        console.log($submitBtn);
                    }
                });
                
                $submitBtn.on('click', function(e){
                    e.preventDefault();
                    $(this).prop('disabled',true);
                    $$._validationName('#result_name');
                    $$._validationEmail('#result_email');
                    $$._validationPhone('#result_contact_number');
                    $submitSection.find('.error').parent().removeClass('has-error');
                    $submitSection.find('.error').remove();
                    console.log($$.errors);
                    if(! $$._hasErrors()) {
                        $$.enableBackDrop();
                        $$.enableLoader({
                            description: 'Processing '+$$.$data.title+' Result, please wait...'
                        });
                        var $params = {
                            name : $('input#result_name').val(),
                            email: $('input#result_email').val(),
                            phoneNumber: $('input#result_contact_number').val(),
                            solution_configurator_id: $$.id,
                            title: $$.$data.title,
                            resultData: JSON.stringify($$.resultData),
                            id: $$.$data.id
                        }
                        $.ajax({
                            url: $$.options.submitUrl,
                            data: $params,
                            method: 'POST',
                            dataType: 'json',
                            cache: false,
                            success: function(data){                               
                                var tpl = _.template($('script#_tmpl_assessment_result_view').html()),
                                $result_view = $(tpl(data));                        
                                $$.animateCss($$.element.find('.panel-heading'),'bounceOutRight',function(obj){
                                    obj.hide();
                                });
                                $$.animateCss($$.$body,'bounceOutRight',function(obj){
                                    obj.hide();
                                });
                                $$.animateCss($$.$footer,'bounceOutRight',function(obj){
                                    obj.hide();
                                });
                                $$.element.html($result_view);
                                $('body > .assessment-loader').remove();    
                                $('.modal-backdrop').remove();
                                $('html,body').animate({
                                    scrollTop: $$.element.offset().top - $$.options.scrollTopOffset
                                }, 500);
                            }
                        });
                    }
                    else {
                        _.each($$.errors, function(x){
                            $(x.element).parent().addClass('has-error')
                                .append('<span class="error">'+x.msg+'</span>');
                        });
                        $submitBtn.prop('disabled',false);
                        $$.errors=[];
                    }
                    
                    console.log(JSON.stringify($$.resultData));
                });
                this.$footer.html($submitSection);
            }
        },
        _renderNestedItem: function($parent, data) {
            var nestedContainer = _.template($('script#_tmpl_assessment_nested_row').html()),
            $$ = this, $nestedContainer,
            item = data.nextQuestion;
    
            item.parent_ans_id = data.id;
            $nestedContainer = nestedContainer({
                title: this.$data.title,
                index: 0,
                item: item
            });
            $nestedContainer = $($nestedContainer);
            var $nestedSection = $parent.find('.nested-items-section').empty().html($nestedContainer),
            $ansContainer = $nestedSection.find('.answer');
            this._prepareAnswer(0,data.nextQuestion,$ansContainer); 
//            console.log('data',data.nextQuestion);
//            console.log('ansContainer',$ansContainer);
//            console.log('nestedItem ',$parent, $nestedContainer);
            if(this.options.animate) {
                $ansContainer.hide();
                $nestedSection.find('.question-item').empty().typed({
                    strings:[item.title],
                    typeSpeed: 20,
                    showCursor:false,
                    callback: function() {
                        $$.animateCss($ansContainer,'bounceInLeft')
                        $ansContainer.show();
                    }
                }); 
            }
            $('html,body').animate({
                scrollTop: $ansContainer.closest('.nested-item').offset().top - this.options.scrollTopOffset
            }, 1000);
            
        },
        _renderItem: function(first) {
            //var $container = this._prepareQuestion(data,$(template.container));
            //this.$body.find('.row').append($container);
            var $$ = this, 
                index = _.first(this.nextIndexs);
                this._removeIndex(index);
            if(!_.isUndefined(index)) {
                var item = this.questionItems[index], 
                container = template.tpl_container({
                    title: this.$data.title,
                    item: item,
                    index: index
                }),
                $container = $(container),
                $ansContainer = $container.find('.answer');
                
                this._prepareAnswer(index,item,$ansContainer);                
                
                if(this.options.animate) {
                    $ansContainer.hide();
                    $container.find('.question-item').empty().typed({
                        strings:[item.title],
                        typeSpeed: 20,
                        showCursor:false,
                        callback: function() {
                            $$.animateCss($ansContainer,'bounceInLeft')
                            $ansContainer.show();
                        }
                    }); 
                }
                this.$body.append($container);
                if(! first) {
                    $('html,body').animate({
                        scrollTop: this.element.find('#question-idx-'+index).offset().top - this.options.scrollTopOffset
                    }, 1000);
                }
            }
        },        
        _prepareAnswer: function(index, data, container) {
            var selector = '.answer-input.input-'+data.type, _self=this;
            if(template.inputs[data.type]) {
                data.index = index;
                data.id = data.qid;
                data.disabled = false;
                data.nextIndex = data.isNested === 1 ? null :_.first(this.nextIndexs);
                console.log(data);
                var $input = template.inputs[data.type](data);
                container.find(selector).append($input);
                container.data('item',data);
                switch(data.type) {
                    case 'text':
                    case 'dropdown':
                    case 'imageradiolist':
                    case 'radiolist':
                    //data.onChange = this._onChange;
                    //container.on('change',$input,function(e){
                    $input.on('change',function(e){
                        //console.log($(this), $(e.target), e, $(this).data('item'), $(e.target).data('item'))
                        _self._processNext($(e.target), data.type);
                    });
                    break;
                    case 'range':
                        var ansItem = data.items[0],
                        $rangeInput = $input.find('#range-'+ansItem.id);
                        $rangeInput.on('slidestop',function(e,ui){
                            _self._processNext($(e.target), data.type, ui);
                        }); 
                    break;
                }
            }
            return container;
        },
        
        _processNext: function(currentInput,type,ui) {
            var ansValue, item;
            switch(type) {
                case 'dropdown':
                    ansValue = currentInput.find(':selected').data('item');
                    item = currentInput.data('item');
                break;
                case 'imageradiolist':
                case 'radiolist':
                    ansValue = currentInput.data('item');
                    item = currentInput.closest('.list-items').data('item');
                break;
                case 'range':
                    ansValue = currentInput.data('item');
                    ansValue.resultValue = ui.value;
                    item = currentInput.closest('.list-items').data('item');
                break;
            }
            if(!_.isEmpty(item)) {
                if(item.isNested === 0 && this.isNextItem() ) {
                    if(! this.isExists(item.nextIndex)) {
                        this._renderItem();
                    }
                }
                else if(item.isNested === 1) {
                    this._renderNestedItem(currentInput.closest('.answer-item'), ansValue);
                }
                else {
                    this._renderSubmitSection();
                }
                this._setResult(item, ansValue, item.isNested);
            }
            //console.log(item, ansValue);
        },
        
        _setResult: function(item, ans, nested) {
            nested = nested || 0;
            if(_.has(item,'parent_ans_id') && !_.isNull(item['parent_ans_id'])) {
                //console.log(item);
                //var ansParentId = item.parent_ans_id;
                _.find(this.resultData, function(x){
                    //console.log('nested map',x.resultData.nextQuestion.parent_ans_id, item.parent_ans_id);
                    if(x.resultData.nextQuestion.parent_ans_id === item.parent_ans_id) {
                        x.resultData.nextQuestion.resultData = ans;
                        return;
                    }
                });
            }
            else {
                this.resultData[item.id] = _.find(this.questionItems, function(x){
                    //console.log('ans',ans);
                    if(x.id === item.id) {
                        x.resultData = ans;
                    }
                    return x.id === item.id;
                });
            }
           console.log('resultMapping:: ', this.resultData);
        },
        
        isNextItem: function() {
            return !!this.nextIndexs.length;
        },
        animateCss: function ($selector, animationName, cb) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $($selector).addClass('animated ' + animationName).one(animationEnd, function() {
                $($selector).removeClass('animated ' + animationName);
                if(typeof cb === 'function') {
                    cb($selector);
                }
            });
        },
        isExists: function(index) {
            var id = '#question-idx-'+index;
            //console.log('cccccccccc',this.$body.find(id), this.$body.find(id).length > 0);
            return this.$body.find(id).length > 0;
        },
        enableBackDrop: function() {
            $('body').append('<div class="modal-backdrop fade in"></div>');
        },
        removeBackDrop: function() {
            $('body > .model-backdrop').remove();
        },
        enableLoader: function(options) {
            var loader =_.template(jQuery('script#_tmpl_brand_loader').html()),
                $loader = $(loader(options));
            $loader.addClass('assessment-loader');
            $('body').append($loader);
        }
    };
    
    $.fn.dpAssessment = function(option){
        return this.each(function(){
            var $this = $(this), 
            data = $this.data(pluginName),
            options = typeof option === 'object' && option;
            if(!data) {
                $this.data(pluginName,(data = new Assessment(this, $.extend({},$.fn[pluginName].defaults,options))));
            }
            if (typeof option === 'string') {
                data[option].call($this);
            }
        });
    };
    $.fn[pluginName].defaults = {
        enableScroll: true,
        data: {},
        submit: false,
        from: 'From',
        to: 'You',
        animate: false,
        scrollTopOffset:55,
        submitUrl: null
    };    
    $.fn.Constructor = Assessment;
    
}(jQuery);